export default function({ store, redirect }) {
    // If the user is not authenticated
    if (!store.state.authenticated) {
        console.log("No tiene acceso a esta ruta")
        return redirect('/')
    } else {
        console.log("No redireccionar")
    }
}