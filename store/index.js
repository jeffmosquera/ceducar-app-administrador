import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios"
import VuexPersist from 'vuex-persist'



const vuexPersist = new VuexPersist({
    key: 'ceducar',
    storage: localStorage
})

const jwtJsDecode = require('jwt-js-decode');


Vue.use(Vuex)

const store = () => {
    return new Vuex.Store({
        strict: true,
        state: {
            authenticated: false,
            cargando: {
                titulo: "Ceducar",
                estado: false
            },
            administrador: {},
            usuarios: [],
            token: {},
            marcaciones: [],
            entradas: [],
            salidas: [],
            salidas_almuerzo: [],
            regresos_almuerzo: []
        },
        mutations: {
            signin: state => state.authenticated = true,
            signout: state => state.authenticated = false,

            cleanStates(state) {
                state.administrador = {}
                state.usuarios = {}
                state.token = {}
            },

            guardarToken(state, payload) {
                state.token = payload
            },
            guardarAdministrador(state, payload) {
                state.administrador = payload
            },

            guardarUsuarios(state, payload) {
                state.usuarios = payload
            },

            guardarMarcaciones(state, payload) {
                state.marcaciones = payload
            },

            guardarEntradas(state, payload) {
                state.entradas = payload
            },

            guardarSalidas(state, payload) {
                state.salidas = payload
            },

            guardarSalidaAlmuerzo(state, payload) {
                state.salidas_almuerzo = payload
            },

            guardarRegresoAlmuerzo(state, payload) {
                state.regresos_almuerzo = payload
            },



            mostrarCargando(state) {
                state.cargando.estado = true
            },

            cerrarCargando(state) {
                state.cargando.estado = false
            },

        },
        actions: {
            action_login(context, payload) {
                return new Promise((resolve, reject) => {
                    let formData = new FormData();
                    formData.append('correo', payload.email);
                    formData.append('clave', payload.password);
                    axios({
                        method: "POST",
                        url: process.env.baseUrl + "/administradores/login",
                        headers: { 'Content-Type': 'multipart/form-data' },
                        data: formData
                    }).then(async result => {
                        context.commit('guardarToken', result.data.data)
                        context.commit('signin')
                        let jwt = jwtJsDecode.jwtDecode(result.data.data.access_token)
                        await context.dispatch("conseguirAdministrador", jwt.payload.identity._id)
                        resolve()
                    }, error => {
                        console.log(error + " ErrorLogin")
                        reject(error.response.data)
                    });
                })
            },

            action_reporte(context, payload) {
                return new Promise((resolve, reject) => {
                    let formData = new FormData();
                    formData.append('correo', context.state.administrador.correo);
                    axios({
                        method: "POST",
                        url: process.env.baseUrl + "/reportes/" + payload,
                        headers: { 'Content-Type': 'multipart/form-data' },
                        data: formData
                    }).then(async result => {
                        resolve(result.data)
                    }, error => {
                        reject()
                    });
                })
            },

            action_logout(context) {
                context.commit('signout')
            },

            async conseguirAdministrador(context, payload) {
                await axios({
                    method: "GET",
                    url: process.env.baseUrl + "/administradores/" + payload,
                    headers: {
                        'Content-Type': "application/json",
                        'Authorization': "Bearer " + context.state.token.access_token
                    },
                }).then(result => {
                    context.commit('guardarAdministrador', result.data.data)
                }, error => {
                    console.log(error + " ErrorLogin")
                });
            },

            async conseguirUsuarios(context) {
                await axios({
                    method: "GET",
                    url: process.env.baseUrl + "/usuarios",
                    headers: {
                        'Content-Type': "application/json",
                        'Authorization': "Bearer " + context.state.token.access_token
                    },
                }).then(result => {
                    context.commit('guardarUsuarios', result.data.data)
                }, error => {
                    console.log(error + " ErrorConseguirUsuarios")
                });
            },

            async conseguirMarcaciones(context) {
                await axios({
                    method: "GET",
                    url: process.env.baseUrl + "/marcaciones",
                    headers: {
                        'Content-Type': "application/json",
                        'Authorization': "Bearer " + context.state.token.access_token
                    },
                }).then(result => {
                    context.commit('guardarMarcaciones', result.data.data)
                }, error => {
                    console.log(error + " ErrorConseguirMarcaciones")
                });
            },

            async conseguirEntradas(context) {
                await axios({
                    method: "GET",
                    url: process.env.baseUrl + "/marcaciones/entrada",
                    headers: {
                        'Content-Type': "application/json",
                        'Authorization': "Bearer " + context.state.token.access_token
                    },
                }).then(result => {
                    context.commit('guardarEntradas', result.data.data)
                }, error => {
                    console.log(error + " ErrorConseguirEntradas")
                });
            },

            async conseguirSalidas(context) {
                await axios({
                    method: "GET",
                    url: process.env.baseUrl + "/marcaciones/salida",
                    headers: {
                        'Content-Type': "application/json",
                        'Authorization': "Bearer " + context.state.token.access_token
                    },
                }).then(result => {
                    context.commit('guardarSalidas', result.data.data)
                }, error => {
                    console.log(error + " ErrorConseguirSalidas")
                });
            },

            async conseguirSalidaAlmuerzo(context) {
                await axios({
                    method: "GET",
                    url: process.env.baseUrl + "/marcaciones/salida_almuerzo",
                    headers: {
                        'Content-Type': "application/json",
                        'Authorization': "Bearer " + context.state.token.access_token
                    },
                }).then(result => {
                    context.commit('guardarSalidaAlmuerzo', result.data.data)
                }, error => {
                    console.log(error + " ErrorConseguirSalidaAlmuerzo")
                });
            },

            async conseguirRegresoAlmuerzo(context) {
                await axios({
                    method: "GET",
                    url: process.env.baseUrl + "/marcaciones/regreso_almuerzo",
                    headers: {
                        'Content-Type': "application/json",
                        'Authorization': "Bearer " + context.state.token.access_token
                    },
                }).then(result => {
                    context.commit('guardarRegresoAlmuerzo', result.data.data)
                }, error => {
                    console.log(error + " ErrorConseguirRegresoAlmuerzo")
                });
            },

            agregarUsuario(context, payload) {
                return new Promise((resolve, reject) => {
                    let formData = new FormData();
                    formData.append('nombre', payload.nombre);
                    formData.append('cedula', payload.cedula);
                    formData.append('telefono', payload.telefono);
                    formData.append('clave', payload.clave);
                    formData.append('correo', payload.correo);
                    formData.append('huella_id', payload.huella_id);
                    axios({
                        method: "POST",
                        url: process.env.baseUrl + "/usuarios",
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            'Authorization': "Bearer " + context.state.token.access_token
                        },
                        data: formData
                    }).then(result => {
                        resolve()
                    }, error => {
                        console.log(error + " ErrorAgregarUsuario")
                        reject(error.response.data)
                    });
                })
            },

            actualizarUsuario(context, payload) {
                return new Promise((resolve, reject) => {
                    let formData = new FormData();
                    formData.append('nombre', payload.nombre);
                    formData.append('cedula', payload.cedula);
                    formData.append('telefono', payload.telefono);
                    formData.append('clave', payload.clave);
                    formData.append('correo', payload.correo);
                    formData.append('huella_id', payload.huella_id);
                    axios({
                        method: "PUT",
                        url: process.env.baseUrl + "/usuarios/" + payload._id,
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            'Authorization': "Bearer " + context.state.token.access_token
                        },
                        data: formData
                    }).then(result => {
                        resolve()
                    }, error => {
                        console.log(error + " ErrorActualizarUsuario")
                        console.log(error.response.data)
                        reject(error.response.data)
                    });
                })
            },

            async eliminarUsuario(context, payload) {
                await axios({
                    method: "DELETE",
                    url: process.env.baseUrl + "/usuarios/" + payload,
                    headers: {
                        'Content-Type': "application/json",
                        'Authorization': "Bearer " + context.state.token.access_token
                    },
                }).then(result => {}, error => {
                    console.log(error + " ErrorEliminarUsuario")
                });
            },

        },
        plugins: [vuexPersist.plugin]
    })

}

export default store