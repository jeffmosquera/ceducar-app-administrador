import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';

const options = {
    confirmButtonColor: 'blue',
    cancelButtonColor: '#ff7674'
}

Vue.use(VueSweetalert2, options)